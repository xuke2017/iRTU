# iRTU 开源DTU/RTU解决方案

基于合宙air202/208/800/801/720/724/H/D/G/U 系列模块的开源DTU/RTU解决方案

已成功运行在上百万设备, 安全可靠, 实力担当!

## 主要功能

1. 支持TCP/UDP socket,支持HTTP,MQTT,等常见透传和非透传模式
2. 支持OneNET,阿里云，百度云，腾讯云等常见公有云。
3. 支持RTU主控模式
4. 支持数据流模版
5. 支持消息推送(电话，短信，网络通知)
6. 支持GPS数据以及相关数据采集
7. 支持ADC,I2C等外设，可以方便的扩展为屏幕、二维码等解决方案.
8. 需要将配置文件烧录到固件的，修改源码irtu.cfg文件，然后打包源码+lib+core 成固件即可; irtu.cfg 内包含demo，可以用web导出的配置json文件替换''(单引号内的json字符串)即可。

## 相关码云库

1. 合宙Air724U模块, 4G cat.1 https://gitee.com/openLuat/Luat_Lua_Air724U
2. 合宙Air720S模块, 4G cat.4 https://gitee.com/openLuat/Luat_CSDK_Air720S


## Wiki 和 Doc 网站

* http://wiki.openluat.com/
* http://doc.openluat.com/

## 视频教程

* iRTU快速接入教程 https://www.bilibili.com/video/av41012302
* iRTU远程浇花视频 https://www.bilibili.com/video/av47478475
* Luat相关工具教程 https://www.bilibili.com/video/av50453083
* Luat硬件设计参考 https://www.bilibili.com/video/av45341487
* Luat开发视频教程 https://www.bilibili.com/video/av50827315

合宙官网 http://www.openluat.com

## 授权协议

[MIT License](LICENSE)
